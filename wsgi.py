import os, sys

MAIN_PATH = os.path.realpath(os.path.dirname(__file__)) + "/.."
sys.path.append(MAIN_PATH)
if not "DJANGO_SETTINGS_MODULE" in os.environ or \
  not os.environ['DJANGO_SETTINGS_MODULE']:
    # change with your project name
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mychimere_project.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

